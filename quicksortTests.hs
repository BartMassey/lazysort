-- Copyright © 2018 Bart Massey
-- [This program is licensed under the "MIT License"]
-- Please see the file LICENSE in the source
-- distribution of this software for license terms.


-- Quicksort tests.

import Control.Monad
import Data.List
import Test.QuickCheck

import Quicksort

partitionListSound :: [Int] -> Bool
partitionListSound [] = True
partitionListSound l =
    let (pivotIndex, l') = partitionList l in
    let (front, back) = splitAt (pivotIndex + 1) l' in
    all (<= last front) front && all (> last front) back

quicksortSound :: [Int] -> Bool
quicksortSound l = quicksort l == sort l

main :: IO()
main = do
  let tests = [ ("partitionList", partitionListSound),
                ("quicksort", quicksortSound) ]
  forM_ tests $ \(name, fn) -> do
    putStrLn $ name ++ ":"
    quickCheck $ withMaxSuccess 10000 fn
