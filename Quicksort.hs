{-# LANGUAGE BangPatterns #-}
-- Copyright © 2018 Bart Massey
-- [This program is licensed under the "MIT License"]
-- Please see the file LICENSE in the source
-- distribution of this software for license terms.


-- | Haskell implementation of in-place Quicksort
-- with median-of-three partitioning.

module Quicksort (partitionList, quicksort, quicksortHead)
where

import Control.Monad
import Control.Monad.ST.Lazy
import Data.Array.ST

listArray :: [t] -> ST s (Int, STArray s Int t)
listArray l = do
  let n = length l
  a <- newListArray (0, n - 1) l
  return (n, a)

exchange :: STArray s Int t -> Int -> Int -> ST s ()
{-# INLINE exchange #-}
exchange a !i !j = do
  ai <- readArray a i
  aj <- readArray a j
  writeArray a i aj
  writeArray a j ai

partitionStep :: Ord t => STArray s Int t -> t -> Int -> Int -> ST s Int
partitionStep a !pivot !start !end
    | start == end = do
        aFinal <- readArray a start
        let pivotIndex =
                case aFinal <= pivot of
                  True -> start
                  False -> start - 1
        return pivotIndex
    | otherwise = do
        aStart <- readArray a start
        aEnd <- readArray a end
        case () of
          () | aStart <= pivot -> partitionStep a pivot (start + 1) end
          () | aEnd > pivot -> partitionStep a pivot start (end - 1)
          () -> do
            exchange a start end
            partitionStep a pivot start (end - 1)

sortThree :: Ord t => (t, t, t) -> (t, t, t)
sortThree t@(a, b, c)
    | a > b = sortThree (b, a, c)
    | b > c = sortThree (a, c, b)
    | otherwise = t

medianOfThree :: Ord t => STArray s Int t -> Int -> Int -> ST s t
medianOfThree a start end = do
  let i1 = start
  let i2 = start + (end - start - 1) `div` 2
  let i3 = end - 1
  v1 <- readArray a i1
  v2 <- readArray a i2
  v3 <- readArray a i3
  let (vi1, vi2@(vPivot, _), vi3) =
          sortThree ((v1, i1), (v2, i2), (v3, i3))
  let three' = [(vi1, i2), (vi2, i1), (vi3, i3)]
  forM_ three' $ \((val, i), i') -> do
    unless (i == i') $ writeArray a i' val
  return vPivot

partitionArray :: Ord t => STArray s Int t -> Int -> Int -> ST s Int
partitionArray a start end =
  case end - start of
    0 -> error "partitionArray: attempt to partition empty array"
    1 -> return start
    2 -> do
      first <- readArray a start
      second <- readArray a (start + 1)
      case compare first second of
        LT -> return start
        EQ -> return (start + 1)
        GT -> do
           exchange a start (start + 1)
           return start
    _ -> do
      vPivot <- medianOfThree a start end
      pivotIndex <- partitionStep a vPivot (start + 1) (end - 1)
      exchange a start pivotIndex
      return pivotIndex

partitionList :: Ord t => [t] -> (Int, [t])
partitionList [] = error "partitionList: attempt to partition empty list"
partitionList l@[_] = (0, l)
partitionList l = runST $ do
  (n, a) <- listArray l
  pivotIndex <- partitionArray a 0 n
  l' <- getElems a
  return (pivotIndex, l')

quicksortArray :: Ord t => STArray s Int t -> Int -> Int -> ST s ()
quicksortArray a start end
    | end - start <= 1 = return ()
    | otherwise = do
        pivotIndex <- partitionArray a start end
        quicksortArray a start pivotIndex
        quicksortArray a (pivotIndex + 1) end

quicksort :: Ord t => [t] -> [t]
quicksort [] = []
quicksort l@[_] = l
quicksort l = runST $ do
  (n, a) <- listArray l
  quicksortArray a 0 n
  getElems a

quicksortHead :: Ord t => [t] -> t
quicksortHead [] = error "quicksortHead: empty list"
quicksortHead [e] = e
quicksortHead l = runST $ do
  (n, a) <- listArray l
  quicksortArray a 0 n
  readArray a 0
