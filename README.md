# "Lazy" Sorting in Haskell
Copyright © 2018 Bart Massey  
2018-02-03

There was an interesting
[blog post](http://www.usrsb.in/selling-laziness.html) by
Alex Beal linked on Reddit couple of days ago touting the
advantages of Haskell's laziness.

One claim in particular caught my eye: the suggestion that
lazy sorting means O(*n*) rather than O(lg *n*) selection of
a value near the beginning of a sorted list.

[*Note:* I've idealized history a bit in this account.  Some
false starts and complications that are totally my fault
have been omitted.]

"Ah!" I said to myself. "This is a claim that can be
tested." So I dusted off my Haskell compiler to check it
out. Metaphorically and almost literally. I hadn't installed
GHC on my new laptop in the four or five months I'd had it,
so the first adventure was:

    apt-get update
    apt-get install ghc

10 minutes or so later I was ready to try this
thing. Easy-peasy.

I fired up `ghci` and had a session that looked like this:

```
    $ ghci
    GHCi, version 8.0.2: http://www.haskell.org/ghc/  :? for help
    Prelude> import Data.List as L
    Prelude L> :set +s
    Prelude L> let input = reverse [1..10000000]
    (0.01 secs, 59,752 bytes)
    Prelude L> last input
    1
    (0.98 secs, 960,063,600 bytes)
    Prelude L> last input
    1
    (1.00 secs, 960,063,376 bytes)
    Prelude L> sum input
    50000005000000
    (2.97 secs, 1,852,373,536 bytes)
    Prelude L> sum input
    50000005000000
    (2.64 secs, 1,852,377,016 bytes)
    Prelude L> sum input
    50000005000000
    (2.64 secs, 1,852,376,808 bytes)
    Prelude L> head $ sort input
    1
    (1.37 secs, 1,200,061,320 bytes)
    Prelude L> head $ sort input
    1
    (1.42 secs, 1,200,065,304 bytes)
    Prelude L> head $ sort input
    1
    (1.13 secs, 1,200,061,120 bytes)
    Prelude L> last $ sort input
    10000000
    (1.33 secs, 1,200,069,336 bytes)
    Prelude L> last $ sort input
    10000000
    (1.44 secs, 1,200,069,360 bytes)
    Prelude L> last $ sort input
    10000000
    (1.22 secs, 1,200,069,320 bytes)
    Prelude L> last $ sort input
    10000000
(1.34 secs, 1,200,069,472 bytes)
```

So. Looked like the same speed to me. Maybe the `last` was a
hair slower, but `last` itself is a hair slower. Certainly
not the factor of lg *n* slowdown I was promised on the tin.
Also, the run-to-run variance was pretty bad. Probably
caused by the 1.2GB of garbage hitting the GC at random
times each run.

But wait! Maybe it's just that I'm not getting any
optimizations, so no loop-fusion bleah bleah bleah. Time to
compile an optimized version of this and check the timings.

Timing compiled Haskell things is hard, because laziness and
GC. You have little control of what is cached between
calls. Nonetheless, you need to iterate to get averages,
because the GC will kick in at random times.  Long and short
of it is probably don't try to just roll your own with
timers.

After digging around, I chose the famous Criterion package
to bench my stuff. So now it was time to install that.

    apt-get install cabal-install
    cabal update
    cabal install cabal-install
    cabal install criterion

Watched that for 10 minutes and was ready to go.

So I set up Criterion the way somebody described in a blog
post and ran `ghc -O4 --make -o lazysort lazysort.hs`.  The
results are in the repo. The HTML graphics were beautiful, and
provided a clear description of what was going on. You can
see the source code in the repo at the tag
`criterion-bogus`, and the report at `criterion-bogus.html`.
For my 1M-element input list, the bottom line was about
like this:

    last            170ns
    head $ sort     170ns
    last $ sort     200ns

Now, honestly, if GHC-compiled Haskell could walk a
1M-element list in 170ns, much less sort it, I'd probably
never write anything other than Haskell ever again.
Obviously, there was **something wrong** here.

Another 15 minutes of careful meditation and
documentation-pondering, and I tried it a different
way. Probably even got Criterion set up correctly this time.
See the source tag `criterion` and the file `criterion.html`
for the artifacts. Now the timings were:

    last            3.7ms
    head $ sort     44ms
    last $ sort     48ms
    
The variances were still quite large: 20%-ish. The `last $
sort` time is almost exactly the sum of the `head $ sort`
time and the `last` time. Bottom line: nope. The system sort
is not lazy.

Confession time: I *knew* that. Or at least strongly
suspected. I've seen the merge-sort code in the Haskell
libraries. But it's always good to check: maybe there was
something I didn't understand that would save me.

Having eventually recalled what I originally started on this
journey for, the next step was to grab a Quicksort
implementation from Hackage and try this benchmarking with
that. Nope. Hackage doesn't have any Quicksort that I can
find. So…time to write a Quicksort package.

Many hours later, I am stuck on a final bug in my pretty
Quicksort package. Partitioning works, but Quicksort fails
for some reason I can't explain. Debugging is
nigh-impossible, because everything is buried in the ST
monad to get mutable arrays.

I think I'm done for now. I'm not sure whether Haskell
laziness is a win here. I'm sure it's unutterably painful to
find out.

-----

OK, after walking away for a half-hour for the second time
and thinking carefully, I found my bug. Couple of wrong
indices passed during an update. Not sure how I would have
found it expect straight inspection and self-griefing. I was
back in business.

Went to run the benchmarks again, but there was a problem:
passing a reversed list of 1M elements to 0-pivot
Quicksort. Runtime was…not OK. So: median-of-three.

After probably four hours of coding and debugging, got
median-of-three partitioning to pass tests. However
`quicksort`, untouched from previous working version, was
now failing. This is a *simple* piece of code.  The failure
seemed like it almost had to be in `partitionArray` even
though `partitionList` passes a lot of tests, but
hand-walking a Quickcheck counterexample through with
`partitionList` and `quicksort` wasn't making it obvious.

-----

Another three hours. Found the bug in my median-of-three
code: was missing an offset. Bug only appeared when
`partitionArray` took a non-zero `start`, so `partitionList`
worked fine.

Ran Criterion again: still stupid slow. 150µs for built-in
sort on 10,000 elements, 500ms for my Quicksort.

Tried turning on profiling. Hooray, adventure! Took a couple
hours to get it all working. Got to rebuild my whole GHC
install to be able to profile something with Criterion in
it. Looks like `partitionStep` is slow and generated a lot
of garbage — I did not expect that. (I totally expected
that.) The profile output file was so wide I had to cut my
font size on my 2560px-wide monitor to be able to get
entries on a single line. My vision is not so good. A bunch
of call centers appeared to be uncounted.

Tried inserting some strictness. Nothing.

Tried closing off the module and inlining things. Nothing.

Built a little `sort` filter command so that I didn't have
to deal with the Criterion stuff in the profiling. Ran it on
pieces of a shuffled dictionary. Looked like runtime hit a
cliff at about 5000 elements. GC finally kicking in hardcore?

I'd heard Vector is faster than STArray, so I thought maybe
I'd convert Quicksort to use that. It really shouldn't help:
it's not like there's fusion available or anything. But
maybe STArray is just terrible. A branch and some
code-munging later, I pretty well had everything moved over.

Then I hit a jaw-dropping compiler error. There was a stray
`getElems` in `quicksortVector`, which meant it was also in
`quicksortArray` in the STArray version. Uh. I'd been
staring at that leftover debugging code for hours without
seeing it. I was being dumb on so many levels, but
code-blindness is a thing after working on a piece of code
that long.

Why hadn't the compiler warned me? I had the vague memory
that `-Wall` was on by default or just didn't exist in GHC.
Apparently that vague memory was incorrect? Turned on
warnings and removed the `getElems` and another stray
inconsequential `readArray` and cleaned up the code a bit.
Ran Quickcheck tests again (another few minutes while I
reinstalled it from the previous adventures) and the tests
all passed. Time to re-profile.

Hooray! 500ms before, 2ms now. Clearly GHC "knew" that the
`getElems` result was dead, since it issued a warning about
exactly that. But apparently the compiler couldn't just
elide the call, even with `-O4`, because reasons. The Vector
version was the same speed, as expected, so I abandoned it
for now. (See the vector branch in the repo.)

My Quicksort was still 30x slower than the builtin
Mergesort. Headed this far down the road, might as well keep
going. It was time to try some optimizations again, now that
they might actually do something.

First step was to replace the use of the standard sort
function from Data.List in `medianOfThree`. This was
unlikely to give any speedup, but it had been bugging me.  I
hard-coded a three-element sort and swizzled some stuff
around.  Sure enough, no noticeable speedup, but at least my
Quicksort didn't depend on somebody else's Mergesort
anymore.

Next step was to try inlining and strictness again. Banged
some stuff and inlined `exchange`. No performance difference.

Switched back to the vector branch and tried replacing the
array accesses in `partitionStep` with unsafe ones. No
performance difference.

Quicksort is known to be inefficient on small arrays. I
could try using the system Mergesort for small arrays, but
it only runs on list. Or I could write my own Heapsort. Ugh.

For comparison, I benched an equivalent Python Quicksort I
had lying around. It ran in 10ms on a 10K-element sorted
array. So 3x slower. Running with PyPy got it in
500µs.

As the code stood, there was no measurable performance
difference between taking the first and last element of a
Quicksorted array. I had been meaning to make Quicksort use
ST.Lazy. Maybe the strictness of the default ST monad was
hiding the problem.

As expected, ST.Lazy slowed down Quicksort by almost a
factor of three, but that wasn't the point. It turns out
that taking the head of my Quicksorted 10000-element array
was now about 10% faster than taking the tail. The file
`criterion-final.html` shows the details. Hardly the O(*n*
lg *n*) → O(*n*) speedup promised on the tin, but better
than nothing, I guess.

Later, in the shower, I was thinking about why GHC hadn't
been able to elide `getElems`. The obvious answer was that
the compiler couldn't tell that `getElems` wouldn't modify
the array, so it had to call it for its side effect. Fair
enough. But why did this call *do* anything? Since the
result was not referenced, `getElems` should have been
called for its side-effect and instantly returned a thunk
for the element list, right? Oops. Bet `getElems` is not
lazy.

One "specialized just return the first element of Quicksort"
later, it turns out — you guessed it — no improvement. I
lost the bet, apparently.

At this point, I was done. I had done all the optimization I
was willing to do.

-----

I don't expect to write much new code in Haskell in the
future. If you're still not sure why, please see above. I'd
guess I spent 10-15 hours on this little project.

I'm really enjoying Rust, which gives a lot of the benefits
of Haskell in practice while avoiding many of its issues.
Haskell has some fundamental problems that keep me away from
it these days:

* Haskell's type system is quite good at catching bugs at
  compile time. GHC has the usual warnings as well, if you
  remember to turn them on. That said, algorithm bugs that
  are not caught are *extremely* difficult to diagnose in
  practice. The new debugger improvements don't seem to help
  much, and the lack of "printf debugging" is just a
  perpetual problem.

* Haskell's performance is wildly unpredictable by mortals
  such as myself, and performance problems are *extremely*
  difficult to diagnose and debug. This example runs at
  least an order of magnitude slower than it should, and I
  don't know why.

* Haskell requires a knowledge of an *immense* amount of
  arcana to accomplish anything. I know of no other language
  that makes things this hard. I've been programming in
  Haskell quite a bit for over a decade, and I still don't
  have what I need to get some simple-seeming tasks
  accomplished with minimal effort.

* Sometimes, as with Quicksort, the natural algorithm is
  imperative. Imperative code seems to me to just be
  inherently slow in Haskell. It's also generally ugly as
  sin. (I don't usually complain about syntax, but
  `readArray` and `writeArray` are a syntactically
  ridiculous way to access imperative arrays.) Debugging
  imperative code in the ST monad is worse than usual,
  because the usual method of trying things on the GHCI
  command line is awkward at best.

I have had big fun with Haskell. I'll keep maintaining the
Haskell I've written. I'll use Haskell for demos and toys
when appropriate. But really, I'm tired of coping with it.

-----

Also, I'm sorry Alex, but I'm really not sold on default
laziness.

There are solutions for explicit laziness in most modern
languages: iterators, generators, futures, etc.  When you
want to write a lazy function in Python, it's usually
super-easy: just stick a `yield` in at the appropriate spot
and you're good to go.

The modularity of explicit laziness nsolutions is pretty
good. Yes, you sometimes get a different interface for the
lazy code than the strict code. In return, you get to not
generate a whole bunch of imaginary lists and risk them
turning into real lists.

The "bounded retry" example is easily handled with explicit
laziness, for example. The retry policy will likely be a
function of the number of retries rather than a list of
values. In my opinion this is clearer anyhow. The
transformation-pipeline example may also be clearer with
explicit laziness.

If it takes me 15 hours to write Quicksort, I'm not getting
Quickselect for free. Even if it were to be performant.

-----

This work is licensed under the "MIT License".  Please see
the file LICENSE in the source distribution of this software
for license terms.
