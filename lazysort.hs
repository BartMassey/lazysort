-- Copyright © 2018 Bart Massey
-- [This program is licensed under the "MIT License"]
-- Please see the file LICENSE in the source
-- distribution of this software for license terms.


-- Benchmark "lazy" sort.

import Criterion.Main
import Data.List (sort)
import Quicksort

bmSize :: Int
bmSize = 10000

setupEnv :: IO [Int]
-- setupEnv = return [bmSize, bmSize-1 .. 1]
setupEnv = return [1..bmSize]

main :: IO ()
main =
  defaultMain [
   env setupEnv $ \a -> bgroup "lazysort" [
    bench "last" $ whnf last a,
    bench "sortHead" $ whnf (head . sort) a,
    bench "sortLast" $ whnf (last . sort) a,
    bench "quicksort+last" $ whnf (last . quicksort) a,
    bench "quicksort+head" $ whnf (head . quicksort) a,
    bench "quicksortHead" $ whnf quicksortHead a
   ]
  ]
